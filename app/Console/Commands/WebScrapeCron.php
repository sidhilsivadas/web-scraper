<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\WebScrapeService;

class WebScrapeCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WEB:SCRAPE';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simple functionality for scraping http://www.mycorporateinfo.com/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(WebScrapeService $WebScrapeService)
    {
        $this->info('Start | '.date("Y-m-d H:i:s"));
        try{
          $WebScrapeService->parseWeb();
        }catch(Exception $e){
          dd($e);
        }

        $this->info("End | ".date("Y-m-d H:i:s"));
    }
}
