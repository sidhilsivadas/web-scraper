<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use KubAT\PhpSimple\HtmlDomParser;
use App\Repositories\IndustryRepository;
use App\Industry;
use App\Company;


class WebScrapeService
{
    public function __construct()
    {
        $this->http_client = new \GuzzleHttp\Client();
    }

    public function getIndustries()
    {
        $industries = IndustryRepository::getIndustries();
        //dd($industries);
        if ($industries->isEmpty()) {
            $this->parseHomePage();
            $industries = IndustryRepository::getIndustries();
        }
        return $industries;
    }

    public function getCompaniesByIndustry($slug)
    {
        $industry = Industry::where(["slug" => $slug])->first();
        $industryId = $industry->industry_id;
        $companies = Company::where(["industry_id" => $industryId])->get();
        if ($companies->isEmpty()) {
            //echo "ddd";die;
            $this->parseCompanyPage($slug);
            $companies = Company::where(["industry_id" => $industryId])->get();
        }
        return $companies;
    }

    public function getCompanyDetails($slug)
    {
        $company = Company::where(["slug" => $slug])->first();
        if (empty($company->information)) {
            $this->parseCompanyDetails($slug);
            $company = Company::where(["slug" => $slug])->first();
        }
        return $company;
    }

    public function parseWeb()
    {
        //$this->parseHomePage();
        //$this->parseCompanyPage("C");
        //$this->parseCompanyDetails("roopa-industries-limited");
    }

    public function parseHomePage()
    {
        $industries = IndustryRepository::getIndustries();
        $industrySlgs = $industries->pluck('slug')->toArray();
        $url = "http://www.mycorporateinfo.com/";
        $response = $this->http_client->request('GET', $url, ['query' => []]);
        $statusCode = $response->getStatusCode();
        $content = (string) $response->getBody();
        $parsedData = [];
        if ($statusCode == 200) {
            if (!empty($content)) {
                $dom = HtmlDomParser::str_get_html($content);
                $industryDom = $dom->find('ul.list-group > li > a');
                //dd(count($industryDom));
                foreach ($industryDom as $industryData) {
                    $href = $industryData->href;
                    $exp = explode("/", $href);
                    if ($exp[1] == "industry") {
                        $text = $industryData->text();
                        $slug = array_slice($exp, -1)[0];
                        if (in_array($slug, $industrySlgs)) {
                            continue;
                        }
                        $parsedData[] = [
                            'slug' => $slug,
                            'industry_name' => $text,
                        ];
                    }
                }
                $this->saveIndustry($parsedData);
            }
        }
    }

    public function parseCompanyPage($industrySlug)
    {
        $industry = Industry::where(["slug" => $industrySlug])->first();
        $industryId = $industry->industry_id;
        $companies = Company::where(["industry_id" => $industryId])->get();
        $companySlgs = $companies->pluck('slug')->toArray();
        $url = "http://www.mycorporateinfo.com/industry/section/$industrySlug";
        $response = $this->http_client->request('GET', $url, ['query' => []]);
        $statusCode = $response->getStatusCode();
        $content = (string) $response->getBody();
        $parsedData = [];
        if ($statusCode == 200) {
            if (!empty($content)) {
                $dom = HtmlDomParser::str_get_html($content);
                $companyDom = $dom->find('table.table.table-bordered > tbody > tr');
                //dd(count($companyDom));
                foreach ($companyDom as $companyData) {
                    $cin = $companyData->childNodes(0)->text();
                    $companyName = $companyData->childNodes(1)->text();
                    $class = $companyData->childNodes(2)->text();
                    $status = $companyData->childNodes(3)->text();
                    if ($companyName == "Company Name") {
                        continue;
                    }
                    $href = $companyData->childNodes(1)->children(0)->href;
                    $exp = explode("/", $href);
                    $slug = array_slice($exp, -1)[0];
                    if (in_array($slug, $companySlgs)) {
                        continue;
                    }

                    $parsedData[] = [
                        "industry_id" => $industryId,
                        "cin" => $cin,
                        "slug" => $slug,
                        "company_name" => $companyName,
                        "class" => $class,
                        "status" => $status,
                        "description" => "",
                        "information" => ""
                    ];
                }
                $this->saveIndustryCompanies($parsedData);
            }
        }
    }

    public function parseCompanyDetails($companySlug)
    {
        $company = Company::where(["slug" => $companySlug])->first();
        //dd($company);
        //$companySlgs = $companies->pluck('slug')->toArray();
        $url = "http://www.mycorporateinfo.com/business/$companySlug";
        $response = $this->http_client->request('GET', $url, ['query' => []]);
        $statusCode = $response->getStatusCode();
        $content = (string) $response->getBody();
        $parsedData = [];
        if ($statusCode == 200) {
            if (!empty($content)) {
                $dom = HtmlDomParser::str_get_html($content);
                $companyDescription = (string) $dom->find('div.main_test > p')[0];
                $companyInformationDom = $dom->find('div#companyinformation > table > tbody > tr');
                $companyInformation = [];
                foreach ($companyInformationDom as $informationData) {
                    $key = $informationData->childNodes(0)->text();
                    $value = $informationData->childNodes(1)->text();
                    $companyInformation[$key] = $value;
                }
                //dd($companyInformation);
                $companyInformation = json_encode($companyInformation);
                Company::where('company_id', $company->company_id)->update(['description' => $companyDescription, 'information' => $companyInformation]);
            }
        }
    }

    public static function saveIndustry($data)
    {
        IndustryRepository::saveIndustry($data);
        //return $data;
    }

    public static function saveIndustryCompanies($data)
    {
        Company::insert($data);
        //return $data;
    }
}
