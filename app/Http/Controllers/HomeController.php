<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\WebScrapeService;

class HomeController extends Controller
{
    public function getIndustries(Request $request, WebScrapeService $webScrapeService)
    {

        $data = $webScrapeService->getIndustries();
        $html = view("webscrape.industries", ['data' => $data])->render();
        return response()->json([
            "data" => $html
        ]);
    }

    public function getCompaniesByIndustry(Request $request, WebScrapeService $webScrapeService)
    {
        $formData = $request->all();
        //dd($formData);
        $data = $webScrapeService->getCompaniesByIndustry($formData['industryId']);
        $html = view("webscrape.companies", ['data' => $data, 'industry' => ""])->render();
        return response()->json([
            "data" => $html
        ]);
    }

    public function getCompanyDetails(Request $request, WebScrapeService $webScrapeService)
    {
        $formData = $request->all();
        //dd($formData);
        $data = $webScrapeService->getCompanyDetails($formData['slug']);
        $html = view("webscrape.company_details", ['data' => $data, 'industry' => ""])->render();
        return response()->json([
            "data" => $html
        ]);
    }
}
