<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('company_id');
            $table->unsignedInteger('industry_id');
            $table->string('cin');
            $table->string('slug');
            $table->string('company_name');
            $table->string('class');
            $table->string('status');
            $table->longText('description');
            $table->longText('information');
            $table->timestamps();
            $table->foreign('industry_id')->references('industry_id')->on('industries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
