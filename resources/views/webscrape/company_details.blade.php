<h3>{{ $data->company_name }}</h3>
<p>{!! $data->description !!}</p>
</br>
<h3>Information</h3>
<table class="table table table-striped">
  
  <tbody>
    @foreach(json_decode($data->information,true) as $key => $value)
    
    <tr>
      
      <td>{{ $key }}</td>
      <td>{{ $value }}</td>
       
    </tr>
    @endforeach
  </tbody>
</table>
