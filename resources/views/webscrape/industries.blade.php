<h3>Industries</h3>
<table class="table table table-striped">
  
  <tbody>
    @foreach($data as $temp)
    
    <tr>
      
      <td><a class="industry-a" href="javascript:" id="{{ $temp->slug }}">{{ $temp->industry_name }}</a></td>
    </tr>
    @endforeach
  </tbody>
</table>