<h3>Companies</h3>
<table class="table table table-striped">
  <thead>
    <tr>
      <th scope="col">CIN</th>
      <th scope="col">Company Name</th>
      <th scope="col">Class</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $temp)
    
    <tr>
      
      <td>{{ $temp->cin }}</td>
       <td><a class="company-a" href="javascript:" id="{{ $temp->slug }}">{{ $temp->company_name }}</a></td>
       <td>{{ $temp->class }}</td>
       <td>{{ $temp->status }}</td>
    </tr>
    @endforeach
  </tbody>
</table>